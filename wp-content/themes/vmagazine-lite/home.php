<?php /* Template Name: home */ ?>
<?php get_header();?>
	<?php wp_head(); ?>
<div class="container home">
	<!-- Container  -->
  <div class="position-relative">
    <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-cover.jpg">
    <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-06.png" class="position-absolute left bottom">
  </div>
  <div class="row bg-white m-0">
    <div class="col-12 col-lg-4 center"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-04.png" class="center"></div>
    <div class="col-12 col-lg-6 center"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-05.png"></div>
  </div>
  <!-- Section3  -->
  <div class="section-3 col-12 bg m-0">
    <div class="row padding">
      <div class="col-12 col-lg-4">
        <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-09.png">
        <p class="text-muted txt-14">Increase the lather of your body wash and remove dead skin cells with an exfoliating nylon towel</p>
      </div>
      <div class="col-12 col-lg-4">
        <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-10.png">
        <p class="text-muted txt-14">Massage the whole body to promote blood circulation and bring relaxation. It transports nutrients and oxygen to the cells</p>
      </div>
      <div class="col-12 col-lg-4">
        <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-11.png">
        <p class="text-muted txt-14">The nylon fabric helps to remove more dirt, oil, and dead skin cells that prevent to irritate sensitive skin and promotes healthy skin</p>
      </div>
    </div>
  </div>
  <!-- Section3  -->
  <!-- Section4  -->
  <div class="section-4 row bg p-5 m-0">
    <div class="col-12 col-lg-6">
			<img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-14.png">
			<p>We make products both for Japan and for export, manufacturing and selling more than 10 million items to delight of our customer each year. We eagerly adopt new materials and always welcome inquires regarding original products. </p>
		</div>
    <div class="col-12 col-lg-6"><iframe width="100%" height="315" src="https://www.youtube.com/embed/_AgdLsuvnZI?controls=0&amp;start=158" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  </div>
  <!-- Section4  -->
  <!-- Section5  -->
  <div class="section-5 row">
    <div class="col-6 d-none d-sm-none d-md-none d-lg-block"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-18.png" class="position-absolute bottom"></div>
    <div class="col-12 col-lg-6">
      <div class="row p-5">
        <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-19.png" class="mt-3 mb-5 col-12">
        <div class="col-12"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-21.png" class="center"></div>
        <div class="col-12 col-lg-6"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-22.png" class="center"></div>
        <div class="col-12 col-lg-6"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-23.png" class="center"></div>
      </div>
    </div>
  </div>
  <!-- Section5  -->
  <!-- Section6  -->
	<div class="section-6 row bg m-0">
		<div class="col-12 col-lg-6"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-26.png" class="center"></div>
		<div class="col-12 col-lg-6 text-white">
			<img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-27.png" class="pb-3 center">
			<p>- Body wash towel , naturally antibacterial and anti-odor</p>
			<p>- Nylon 100% naturally made, all natural materials with no dyes or chemicals used during production</p>
			<p>- Nylon 66 , The weave construction is incredibly unique, and the fabric is very absorbent and quick to dry </p>
		</div>
	</div>
  <!-- Section6  -->
  <!-- Section7  -->
  <div class="section-7 row bg-white m-0 pbt-5" style="top: -45px;position: relative;z-index:0;padding-top: 50px;">
    <div class="col-12 col-lg-6 p-5 text-muted">
			<img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-43.png">
			<p>At Tatusne , we use a veriety of materials to design, manufacture, and sell body scrub towels and more. </p>
			<p>We are actively involved in developing our own products while also serving as a versatile original equipment manufacturer.</p>
		</div>
    <div class="col-12 col-lg-6"><img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/map.jpg"></div>
      <img src="<?php echo get_home_url();?>/wp-content/themes/twentytwenty/assets/images/Body-wash-towel-45.png" class="pt-4 center">
  </div>
  <!-- Section7  -->

<!-- Container  -->

</div>
