<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         '3A3EbL3jQKNjB5My6w7XyT3nqCiArff87rBpb6fhdg8sK5VQyvQ1KERTFon1g4ne');
define('SECURE_AUTH_KEY',  'Dq3x8r1rRWFzm09EbTNKWY6RjKMxnIUYTzAVtSqSQPChf2pInGsrEgTwLUvyhJgw');
define('LOGGED_IN_KEY',    'i6XfIqVySWrEKePiqCqmX7viu6AEYv2sEF3xfCothmEDU2Q2wL8wtpQfS3RNuaCy');
define('NONCE_KEY',        'zLQ3wNBXW6JzCrawo6LqwXhXK2v6m9spjd5Fz4YBfGMVmhsfHAXn1u3R99Bnb0SC');
define('AUTH_SALT',        '6VwKfwStr8aALQLwsM9XnJozCTXPQK0VWg1Ynd9xwVPt778jnyx6sFbbQJ5P3irj');
define('SECURE_AUTH_SALT', '9YYKh0sDsI2jevxRQqPhVJnLKjNoYF16wCuqNzyBPRpEpCTyjLIae6NefqQDTXHK');
define('LOGGED_IN_SALT',   '7qp0idoqfNYiAvj3x7GbGaN4VcNnUooxHeNUGcGs8YjMj8nMj6u4tF2S4iJUaxxY');
define('NONCE_SALT',       'QCcCEjhWgepDPKESE7LVJ4d58H0KNyouFbQdbx7hMxHiE6yKMghB7n1EbB7ua6bG');
